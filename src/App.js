import React, { Component } from 'react';
import './App.scss';
import WeatherCard from './component/weatherCard.js'
import axios from 'axios'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      temp: '',
    }
  }

  componentDidMount() {
    axios.get("https://api.darksky.net/forecast/1196a349ea7c4efacd71bae1baa90c23/42.3601,-71.0589")
      .then(res => {
        const temp = res.hourly.temperature;
        this.setState(temp);
        console.log(temp);
      });
  }
  render() {
    return (
      <WeatherCard temp={this.state.temp} />
      
    );
  }
}

export default App;
