import React from 'react';


const Weathercard = (props) => {
    return (
      <> 
        <div className="navbar">
          <div className="navbarBrand" href="#">Welcome to Trending!</div>
          <div className="category">Name of category here</div>
        </div>
        <div className="m-5"></div>
        <div className="container">
          <div className="card text-center">
            <div className="Card-title">Auburn</div>
            <div className="temp text-center">{props.temp}</div>
            <div className="temp-icon"></div>
            <p className="temp-name">sunny</p>
            <div className="row">
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Sun</p>
              </div>
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Mon</p>
              </div>
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Tue</p>
              </div>
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Wed</p>
              </div>
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Thu</p>
              </div>
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Fri</p>
              </div>
              <div className="col">
                <div className="weekly-Temp"></div>
                <p>Sat</p>
              </div>
            </div>

          </div>
        </div>
      </>
        
    );

}

export default Weathercard;